console.log("Rest API")

// [Section] JavaScript Synchronous vs Asynchronous
	
	// by default, javascript is synchronous meaning it only executes one statement at a time

console.log(`hello world`)

// conole.log(`hello`)

/*for(let i = 0; i<=1500; i++){
	console.log(i)
}*/

console.log(`I am the console.log after the for loop`)

// async and await - it lets us to proceed or execute more than one statements at the same time

// [Section] Getting all post
	// the fetch API that allows us to asynchronously request for a resource or data
		// fetch() method in javascript is used to request to the server and load information on the webpage
	// Syntax:
		// fetch("apiURL")
	
	console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

		/*a "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value */

			// A "promise" may be in one of 3 possible states: fulfilled, rejected and pending.
			/*
				pending: initial states neither fulfilled nor rejected response
				fulfilled: operation was completed
				rejected: operation reject
			*/
		// Syntax
		// fetch("apiURL")
		// .then(response => {what will you do with the response})

		fetch("https://jsonplaceholder.typicode.com/posts")
		// the ".then()" method captures the response object and returns another promise which will be either resolved or rejected
		.then(response => {
			console.log(response)
			return response.json()
		})
		.then(data => {
			let title = data.map(element => element.title)
			console.log(title)
		})

		// The "async" and "await" keyword to achieve asynchronous code

		async function fetchData(){
			let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))
			console.log(result)

			let json = await(result.json())
			console.log(json)
			// await used to perform codes before it proceed
		}
		fetchData()

		// Section - Get a specific post
			// Retrieves a specific post following the REST API(/post/:id)
			// wildcard is where you can put any value, it then create a link between id parameter in the URL and the value provided in the URL
		fetch("https://jsonplaceholder.typicode.com/posts/3", {method: 'GET'})
		.then(response =>{
			return response.json()
		})
		.then(result => {
			console.log(result)
		})

		// [Section] Creating POST
		/*Syntax
			fetch("apiURL", {necessatyOptions})
			.then(response => response.json())
			.then(result => {codeblock})
		*/

		fetch("https://jsonplaceholder.typicode.com/posts", {
			// REST API method to execute
			method: 'POST',
			// headers, it tells us the data type of the request
			headers: {
				'Content-type': 'application/json'
			},
			// body - contains the data that will be added to the database/the request of the user
			body: JSON.stringify({
				title: 'New Post',
				body: "Hello World",
				userId: 1
			})		
		})
		.then(response => {
			return response.json()
		})
		.then(result =>{
			console.log(result)
		})

		// Section - updating a post
		// put - whole document
		fetch("https://jsonplaceholder.typicode.com/posts/1", {
				method: 'PUT',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify({
					title: 'updated post',
					body: 'hello again',
					userId: 1
				})
		})
		.then(response => response.json())
		.then(result => console.log(result))

		// Section - Patch
		// patch - properties

		fetch("https://jsonplaceholder.typicode.com/posts/1", {
				method: 'PATCH',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify({
					title: 'Updated Title'
				})
		})
		.then(response => response.json())
		.then(result => console.log(result))

		// mini-activity
		// using the patch method, change the body of the document which has the id of 18 to "Hello it's me!"

		fetch("https://jsonplaceholder.typicode.com/posts/18", {
				method: 'PATCH',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify({
					body: "Hello it's me"
				})
		})
		.then(response => response.json())
		.then(result => console.log(result))

		// Section - delete a post

		fetch("https://jsonplaceholder.typicode.com/posts/1", {method: 'DELETE'})
		.then(response => response.json())
		.then(result => console.log(result))